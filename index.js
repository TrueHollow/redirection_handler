'use strict';

const config = require('./config');
const express = require('express');
const app = express();

const http_port = process.env.PORT || config.http_port;
const Browser = require('./Browser');

const browser = new Browser();
browser.init().then(() => {
    console.log(`app running on port http://localhost:${http_port}...`);
    app.listen(http_port);
});

app.route('/*').get(async (req, res) => {
    try {
        const url = req.params[0];
        if (!url) {
            res.status(400).json({success: false, err: 'No valid body data.'});
        } else {
            const urlReal = await browser.test(url);
            if (!urlReal) {
                throw 'urlReal undefined';
            }
            if (urlReal !== url) {
                res.send(urlReal);
            } else {
                res.send("0"); // Cannot send just 0
            }
        }
    } catch (e) {
        console.error(e);
        res.status(500).json({success: false, err: 'internal error' });
    }
});

module.exports = app;
