'use strict';
const config = require('./config');

const puppeteer = require("puppeteer-extra");

const pluginStealth = require("puppeteer-extra-plugin-stealth");
puppeteer.use(pluginStealth());

const LONG_DELAY = 5000;

class Browser {
    constructor() {
        this.browser = null;
    }

    async init() {
        this.browser = await puppeteer.launch(config.chrome);
        console.log('App: Initialization, creating new browser');
    }

    async delay() {
        return new Promise(resolve => {
            setTimeout(resolve, LONG_DELAY);
        })
    }

    async getBrowser() {
        return new Promise(resolve => {
            const check = () => {
                if (this.browser) {
                    resolve(this.browser);
                } else {
                    setTimeout(() => check(), 1000);
                }
            };
            check();
        })
    }

    async test(url) {
        let page;
        try {
            const browser = await this.getBrowser();
            page = await browser.newPage();
            await page.setViewport({width: 1920, height: 1040});
            await page.goto(url, {waitUntil: 'networkidle0'});
            await this.delay();
            const urlReal = await page.url();
            await page.close();
            return urlReal;
        } catch (e) {
            if (page) {
                await page.close();
            }
        }
    }

    async clean() {
        if (this.browser) {
            await this.browser.close();
        }
    }
}

module.exports = Browser;
