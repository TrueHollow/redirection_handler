'use strict';

const chai = require('chai');
chai.use(require('chai-http'));
const expect = require('chai').expect;
const app = require('../../index');

const urlPath = '/';
describe('GET ' + urlPath, () => {
    it('test GET redirection', async() => {
        const fullUrl = urlPath + 'http://google.com';
        let res = await chai.request(app).get(fullUrl);
        expect(res).to.have.status(200);
        const {text} = res;
        console.log(JSON.stringify(text));
    });

    it('test GET no redirection', async() => {
        const fullUrl = urlPath + 'https://www.google.com/?gws_rd=ssl';
        let res = await chai.request(app).get(fullUrl);
        expect(res).to.have.status(200);
        const {text} = res;
        console.log(JSON.stringify(text));
    });
});
