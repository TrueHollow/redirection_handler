## Preparation

### Node version

Tested on

```sh
node --version
v11.14.0
```

```json
{
  "chrome": {
    "args": [
      "--no-sandbox",
      "--disable-setuid-sandbox",
      "--disable-infobars",
      "--window-position=0,0",
      "--ignore-certifcate-errors",
      "--ignore-certifcate-errors-spki-list"
    ],
    "headless": false,
    "ignoreHTTPSErrors": true
  },
  "http_port": 3000
}
```

### Install dependencies

Execute command ```npm install```

### Running tests

Execute command ```npm test```

## Running app

Execute command ```npm start```
